# Chuẩn bị gì **trước** khi tới lớp học Python http://pymi.vn ? 🤔

## Cài đặt Python
- Nếu dùng Windows và học Python để làm data analysis hay *học cho vui*, hãy tải WinPython (bộ đóng sẵn nhiều gói cần thiết, bản zero cho kích thước nhỏ hơn): https://winpython.github.io/ - [xem hướng dẫn Tiếng Việt](https://medium.com/pymi/l%E1%BA%ADp-tr%C3%ACnh-python-tr%C3%AAn-windows-v%E1%BB%9Bi-winpython-75b6d6c42d1)
  Hay **Anaconda** (https://www.continuum.io/downloads) - Video hướng dẫn: https://www.youtube.com/watch?v=YJC6ldI3hWk
- [Windows- đặc biệt với Win7] Nếu không được, thử cài trực tiếp
  [Python bản 3.4.x ](https://www.python.org/downloads/release/python-344/). Tải file MSI installer... Thử lần lượt bản x86-64, nếu không được thử lại bản x86
- Nếu muốn trở thành lập trình viên chuyên nghiệp, nên cài hệ điều hành Ubuntu 16.04 (https://www.ubuntu.com/download/desktop) dùng thay Windows (Google hướng dẫn đầy trên mạng). Ubuntu có cài sẵn Python. **Chú ý backup dữ liệu (copy ra chỗ khác: USB, máy khác) trước khi cài Ubuntu, mất đừng kêu 😛)**.  PS: biết code trên Ubuntu +2 triệu tiền lương.
- Nếu không phải hai trường hợp trên, cài đặt Python3 lên máy bản 3.5.2 (https://www.python.org/downloads/release/python-352/) (Windows,OSX/Mac,Linux).
  Lý do sử dụng bản này vì đây là phiên bản có sẵn trên các Ubuntu server 16.04. Sau này khi viết các chương trình Python, các bạn không cần phải lo lắng về phiên bản khi chạy code trên server của mình.
  Cài xong hãy bật chương trình có tên `IDLE` để code.

## Chuẩn bị tài liệu
- Tải sẵn tài liệu ở định dạng HTML https://docs.python.org/3.5/download.html
  giải nén ra và xem trên máy (vào file index.html). Mở sẳn mục "Tutorial" (tương đương với
  xem online tại https://docs.python.org/3/tutorial/index.html). Nếu link die, thử
  lại với link https://docs.python.org/3/download.html
- Xem clip tại https://www.youtube.com/playlist?list=PLqVzNKNeM_HyuAfJYN3msXT8a6EpGLjpL
  để không bị lạ lẫm.

## Tạo tài khoản cần thiết (trước buổi 3)
- Like Facebook page tại https://www.facebook.com/pyfml/ để nhận mọi update mới nhất.
- Đăng ký một tài khoản tại https://gitlab.com/ để xem bài tập, nộp bài làm,
  nhớ tạo username và password mới, không đăng nhập bằng tài khoản Google.
- Học viên tham gia vào GitLab group do lớp cung cấp để có quyền xem bài tập.
- Nhập email vào http://invite.pymi.vn/ để nhận thư mời tham gia forum hỏi đáp Python, Django, Golang, Linux ...

## Cài đặt editor (sau buổi 2) <a name="editor"></a>
Nếu như Microsoft Word là "trình soạn thảo văn bản", thì editor là các
"trình soạn thảo code", trên Windows có sẵn "notepad", nhưng rất đểu, không
khuyến khích. Hướng dẫn chạy code trên Windows https://www.youtube.com/watch?v=UPZvzlfsiaY
PS: clip chạy Python 2, lớp học Python 3.

Cài một editor/IDE tuỳ thích.

- Khuyên dùng VSCode: https://code.visualstudio.com/
- Best editor: KHÔNG TỒN TẠI. Gợi ý: [Sublime Text 3](http://www.sublimetext.com/)
hoặc [VS Code](https://code.visualstudio.com/download) (khác với VisualStudio to nặng).
- Best IDE: Pycharm https://www.jetbrains.com/pycharm/ . No 1, không cần cãi 😎
Nhược điểm: nặng/ ngốn ram. Nếu dùng, chỉ nên dùng từ buổi 7 trở đi - để làm
quen với cách chạy code bằng lệnh trước.
- Chưa đủ phê 😗 Vim (http://www.vim.org/download.php) hoặc Emacs (https://www.gnu.org/software/emacs/download.html)

## Chuẩn bị trước buổi 3
- Cài ipython bằng bất cứ cách nào có thể. Nếu trên máy có cài pip, gõ thử
  `pip install ipython`(Windows) hay `sudo pip3 install ipython`.
  Nếu gặp lỗi `pip: command not found` tức chưa cài pip trên máy.
  Cài pip bằng lệnh: `sudo python3 -m ensurepip`, trên Windows chạy
  `python -m ensurepip`.

- Cài đặt git lên máy (https://git-scm.com/downloads). Git là một công cụ
  có giao diện dòng lệnh (gõ các lệnh để làm việc). Nếu vì lý do gì mà không muốn
  gõ lệnh, hãy tự hỏi tại sao muốn gõ code?
- Ubuntu: `sudo apt-get update; sudo apt-get install -y git tig`
  Khi `apt-get` yêu cầu nhập password, `apt-get` sẽ không hiện password hay dấu
  sao nhưng vẫn đang chờ bạn nhập password rồi gõ enter.
- OSX: cài [`homebrew`](https://brew.sh/), sau đó `brew update && brew install git`
- Windows: lên trang chủ tải về và next next next... Cài xong, chuột phải ra
  desktop, chọn "Git bash here".
- Học sử dụng git và gitlab: https://pymivn.github.io/vinagit/
  và https://try.github.io/
- Cài git chứ không phải cài `GitHub` hay cài `GitLab`. GitHub, GitLab là
  các trang web cung cấp dịch vụ lưu trữ các thư mục chứa code (repo).
  GitHub phổ biến với cộng đồng mã nguồn mở, GitLab cho phép lưu repo bí mật
  (private) mà không mất tiền, vì vậy khi học ở lớp thì dùng GitLab.

## Bật Ubuntu trên Windows 10
Vì một lý do không chính đáng nào đó, bạn không chịu cài Ubuntu, thì bạn có thể
xem cách bật hệ thống Ubuntu thử nghiệm trên Windows 10 tại [đây](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) hay clip https://www.youtube.com/watch?v=DmsJHocTt84&app=desktop

PS: đây là thử nghiệm của Windows 10, có lỗi thì hãy tự tìm cách sửa, chúng tôi
không có kinh nghiệm và không hỗ trợ. Tốt hơn hết là tìm cách cài Ubuntu/Fedora.


## Nếu bạn bỏ lỡ buổi học đầu tiên
Nhớ làm đầy đủ các phần chuẩn bị nói trên (cài python, tải tài liệu).
Buổi đầu lớp học về các kiểu số, boolean, nếu lỡ buổi học có thể đọc các
tài liệu sau để nắm đủ kiến thức.

- http://pymi.vn/tutorial/python-la-gi/
- http://pymi.vn/tutorial/python-integer/
- http://pymi.vn/tutorial/python-calculation-2/
- http://pymi.vn/tutorial/boolean/
- http://pymi.vn/blog/why-not-float/

Tham khảo [bài giảng buổi 1 của khoá
201706](https://gist.github.com/hvnsweeting/fa9b1f43cbac283bb8901123b53c2a2e)

ĐỌC THÊM tài liệu về số:
- https://docs.python.org/3/tutorial/introduction.html#numbers

## Nếu bạn bỏ lỡ buổi học thứ hai
Buổi 2 lớp học về string, list cơ bản. Xem tài liệu sau để nắm đủ kiến thức:
- http://pymi.vn/tutorial/string1/
- http://pymi.vn/tutorial/naming/
- http://pymi.vn/tutorial/unicode/
- Đăng ký một tài khoản Slack tại http://invite.pymi.vn/ để trao đổi / chat
  chit với các đồng môn, nhận thông báo về lớp.

Tham khảo [bài giảng buổi 2 của khoá
201706](https://gist.github.com/hvnsweeting/dee9ed135118c48a764d1793eec669ef)

Đọc thêm tài liệu về string và list:
- https://docs.python.org/3/tutorial/introduction.html#strings
- https://docs.python.org/3/tutorial/introduction.html#lists
